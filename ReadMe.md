# Brunas protocol PHP library

Purpose of the library is to let Brunas clients easily implement carriage data updates using unified Brunas-protocol.
Library only documents the protocol and parses JSON into BrunasProtocol object. Usage of library is not mandatory, only
recommended.

## Dependencies
 - brunas/json (to JSON encode/decode Brunas-protocol carriage)
 - PHP >=8.1
## Usage

### WebHooks

> If you did not yet register webhook on Brunas - contact https://brunas.com

Once you have registered your webhook - you should be able to receive data updates from Brunas for your registered
version of protocol

> Please use the same version of the library as your registered webhook (on fresh registration, it will be the latest
> existing version)

#### Webhook consists of 3 HTTP methods (all on the same registered API endpoint):

- POST - carriage create event (full carriage protocol data is sent via body)
- PUT - carriage update event (full carriage protocol data is sent via body)
- DELETE - carriage delete event (only deleted carriage id is sent via url path parameter)

For example, if your registered endpoint
> https://brunas-client.example.com/api/carriage-updates

then you would receive POST and PUT calls to this endpoint according to the event of carriage.

And you will receive DELETE calls on 
 > https://brunas-client.example.com/api/carriage-updates/{carriageId}

where carriageId is the id of deleted carriage


#### Authorization

Upon registering webhook, you will receive API key. This key will exist in every webhook request in Authorization
header:

> Authorization: Bearer {token}

It is your responsibility to validate this token and ignore/block webhook calls if this key is invalid

Full documentation on webhook is located in file webhook-api.yaml (Open with Swagger or any other OpenAPI viewer)

    Our web server will send data update events consistently one after another (FIFO). We will wait for a response from a webhook and expect a HTTP status code 200. 
    If request fails or HTTP response status code is not 200, request is marked as failed and is re-sent after 5 seconds.
    If second try fails, retry will be made after 1 minute. All further retries will be every 5 minutes.
    All failed requests are logged in our system, we will contact you ASAP after we get a failure notification.

### Parsing

To decode JSON to BrunasProtocol object just use Parser::jsonToCarriage($json)

    try {
        $result = \BrunasProtocol\Parser::jsonToCarriage($json);
    } catch (\Json\JsonDecodeException) {
        //Handle parse exception here
    }


### Testing

In this library you can find script to simulate & test webhook data.
Script is located in _**bin/brunas-protocol-webhook-test.php**_

If you launch this script it will print instructions how to test specific webhook events

    php vendor/bin/brunas-protocol-webhook-test.php

Script allows you to send specific events to your created webhook and test if routing, parsing and authorization is implemented correctly.

## Versioning

Version number is defined by 3 parts separated by a dot "a.b.c" f.e. "1.00.000"

#### Major version (a part)
Major version defines the complete version of the library and will only be changed if whole protocol would be refactored. 
Updating this version should be considered very carefully because it would definitely need some refactoring of the implementation.

#### Minor version (b part)
Minor version describes changes that significantly affects protocol structure. It will be changed whenever updated protocol would not be compatible with previous version. 
For example, if field is renamed or removed from the protocol.
Updating this version also should be done carefully, read patch notes and see the affected protocol elements.

#### Patch version (c part)
Patch version describes minor changes that would not critically affect protocol structure. For example, if we add a new field or make a bugfix.
Updating this version should not break any implementation and can be done occasionally.