<?php


use BrunasProtocol\Carriage;
use BrunasProtocol\CMR;
use BrunasProtocol\Company;
use BrunasProtocol\Country;
use BrunasProtocol\ExpeditionData;
use BrunasProtocol\File;
use BrunasProtocol\Location;
use BrunasProtocol\Price;
use BrunasProtocol\Task;
use BrunasProtocol\TaskType;
use BrunasProtocol\TimeInterval;
use BrunasProtocol\TimeIntervalType;
use BrunasProtocol\TransportedVehicle;
use BrunasProtocol\User;
use BrunasProtocol\Vehicle;
use JetBrains\PhpStorm\NoReturn;
use Json\Json;

$binDir = $_composer_bin_dir ?? __DIR__ . '/../vendor/bin';


include_once $binDir . "/../autoload.php";

#[NoReturn]
function printHelp(string $error): never {
    echo PHP_EOL;
    echo $error . PHP_EOL;
    echo "Use this script like this:" . PHP_EOL;
    echo "php vendor/bin/brunas-protocol-webhook-test.php [endpoint] [event] [apiKey]" . PHP_EOL;
    echo PHP_EOL;
    echo "  [endpoint] - endpoint of your Webhook" . PHP_EOL;
    echo "  [event] - event you want to simulate: create, delete, update" . PHP_EOL;
    echo "  [apiKey] - api key you want to send (you can use this to test your API key validation)" . PHP_EOL;
    echo PHP_EOL;
    echo PHP_EOL;
    echo "Examples:" . PHP_EOL;
    echo "php vendor/bin/brunas-protocol-webhook-test.php http://localhost/api/carriage-update create gr6CP5Z7vayLkBBgkP8groHtks" . PHP_EOL;
    echo "php vendor/bin/brunas-protocol-webhook-test.php https://my.domain.com/api/carriage-update delete stGqbmBdZmS7AsIU1UWx8dQNLB" . PHP_EOL;
    die();
}

if (count($argv) < 4) {
    printHelp("Arguments are missing!");
}

enum Event: string {
    case Create = "create";
    case Update = "update";
    case Delete = "delete";

}

$url = $argv[1];
$event = Event::tryFrom($argv[2]);
if (!$event) {
    printHelp("Invalid [event] value '$argv[2]'");
}
$apiKey = $argv[3];


function makeRequest(string $url, string $method, ?string $body, string $apiKey): void {
    echo "Sending $method request to $url (apiKey:$apiKey)" . PHP_EOL;
    echo "Body: $body" . PHP_EOL;

    $ch = curl_init($url);
    $body && curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type:application/json",
        "Authorization: Bearer $apiKey"
    ));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    echo PHP_EOL;

    echo "Response:" . PHP_EOL;
    echo "Http status:$code. Body:" . PHP_EOL;
    echo $result . PHP_EOL;
}


function createFileFromUrl(string $url): File {
    $result = new File();
    $result->publicUrl = $url;
    return $result;
}

function fakeCarriage(): Carriage {
    $vehicle = new Vehicle();
    $vehicle->id = 15;
    $vehicle->registrationNumber = "ABC123";

    $customer = new Company();
    $customer->id = "C1526";
    $customer->vatCode = "DE12345";
    $customer->name = "Best company";

    $carriage = new Carriage();
    $carriage->customer = $customer;
    $carriage->orderNumber = "BRUNAS123";
    $carriage->id = 1515;

    $price = new Price();
    $price->price = 1650.50;

    $carriage->price = $price;
    $carriage->start = (new DateTime())->sub(new DateInterval("P1D"));
    $carriage->end = (new DateTime())->add(new DateInterval("P1D"));
    $carriage->assignedVehicle = $vehicle;

    $carrier = new Company();



    $expeditionPrice = new Price();
    $expeditionPrice->price = 1200.50;

    $expeditionData = new ExpeditionData();
    $expeditionData->price = $expeditionPrice;
    $expeditionData->invoiceFile = createFileFromUrl("https://dummyfile.com/files/not-working-invoice-file.pdf");
    $expeditionData->orderFile = createFileFromUrl("https://dummyfile.com/files/not-working-order-file.pdf");


    $carrier->id = "M5321";
    $carrier->name = "Test Logistics";
    $carrier->vatCode = "DE555555";
    $expeditionData->carrier = $carrier;


    $carriage->expeditionData = $expeditionData;

    $carriage->files = [createFileFromUrl("https://dummyfile.com/files/not-working-file-1.pdf"), createFileFromUrl("https://dummyfile.com/files/not-working-file-2.pdf"), createFileFromUrl("https://dummyfile.com/files/not-working-file-3.pdf"),];


    $loadingTask = new Task();
    $loadingTask->id = 156489;
    $loadingTask->type = TaskType::Load;
    $loadingTask->date = (new DateTime())->format("Y-m-d");
    $loadingTask->commentForDriver = "Please check if cargo is not damaged";
    $loadingTask->arrivalTime = (new DateTime())->sub(new DateInterval("P1D"));
    $loadingTask->departureTime = (new DateTime())->sub(new DateInterval("P1D"));


    $location = new Location();
    $location->address = "3 Rue Neuve";
    $location->lat = 48.45152;
    $location->lng = 3.37212;
    $location->zipCode = "77114";
    $country = new Country();
    $country->alpha2 = "FR";
    $location->country = $country;
    $location->city = "Villiers-sur-Seine";
    $loadingTask->location = $location;


    $validArrivalTime = new TimeInterval();
    $validArrivalTime->type = TimeIntervalType::Between;
    $validArrivalTime->from = "12:00";
    $validArrivalTime->to = "16:00";

    $loadingTask->validArrivalTime = $validArrivalTime;

    $carriage->tasks[] = $loadingTask;

    $fuelTask = new Task();
    $fuelTask->id = 156490;
    $fuelTask->type = TaskType::FuelStation;
    $fuelTask->date = (new DateTime())->format("Y-m-d");
    $fuelTask->commentForDriver = "PIN of the card is written under the seat";

    $location = new Location();
    $location->address = "70 Rue Aristide Briand";
    $location->lat = 48.51186;
    $location->lng = 3.71997;
    $location->zipCode = "10100";
    $country = new Country();
    $country->alpha2 = "FR";
    $location->country = $country;
    $location->city = "Romilly-sur-Seine";
    $fuelTask->location = $location;


    $carriage->tasks[] = $fuelTask;

    $unloadingTask = new Task();
    $unloadingTask->id = 156491;
    $unloadingTask->type = TaskType::Unload;
    $unloadingTask->date = (new DateTime())->format("Y-m-d");
    $unloadingTask->commentForDriver = "Make su they give you all papers upon leaving";

    $location = new Location();
    $location->address = "Pasing-Obermenzing";
    $location->lat = 48.145386;
    $location->lng = 11.471595;
    $location->zipCode = "81241";

    $country = new Country();
    $country->alpha2 = "DE";
    $location->country = $country;
    $location->city = "Miunchen";
    $unloadingTask->location = $location;


    $validArrivalTime = new TimeInterval();
    $validArrivalTime->type = TimeIntervalType::Before;
    $validArrivalTime->from = "";
    $validArrivalTime->to = "16:00";

    $unloadingTask->validArrivalTime = $validArrivalTime;

    $carriage->tasks[] = $unloadingTask;

    $unloadingTask2 = new Task();
    $unloadingTask2->id = 156492;
    $unloadingTask2->type = TaskType::Unload;
    $unloadingTask2->date = (new DateTime())->format("Y-m-d");
    $unloadingTask2->commentForDriver = "Make su they give you all papers upon leaving";

    $location = new Location();
    $location->address = "Kaufmannstraße 1";
    $location->lat = 50.73089;
    $location->lng = 7.08052;
    $location->zipCode = "53115";
    $country = new Country();
    $country->alpha2 = "DE";
    $location->country = $country;
    $location->city = "Bonn";
    $unloadingTask2->location = $location;


    $validArrivalTime = new TimeInterval();
    $validArrivalTime->type = TimeIntervalType::After;
    $validArrivalTime->from = "13:00";
    $validArrivalTime->to = "";

    $unloadingTask2->validArrivalTime = $validArrivalTime;

    $carriage->tasks[] = $unloadingTask2;


    $cmr1 = new CMR();
    $cmr1->id = 154987;
    $cmr1->loadTaskId = $loadingTask->id;
    $cmr1->unloadTaskId = $unloadingTask->id;
    $cmr1->files = [createFileFromUrl("https://dummyfile.com/files/cmr-1.jpg")];
    $carriage->cmrList[] = $cmr1;

    $cmr2 = new CMR();
    $cmr2->id = 154988;
    $cmr2->loadTaskId = $loadingTask->id;
    $cmr2->unloadTaskId = $unloadingTask2->id;
    $cmr2->files = [createFileFromUrl("https://dummyfile.com/files/cmr-1.jpg")];
    $carriage->cmrList[] = $cmr2;


    for ($i = 1; $i <= 8; $i++) {
        $vin = new TransportedVehicle();
        $vin->id = 78998;
        $vin->VIN = "AAAAAAAAAAAAAAAA1";
        $vin->oemCategory = "X";
        $vin->model = "VW Golf VI";
        $vin->damageFiles = [createFileFromUrl("https://dummyfile.com/files/damage-file-1.jpg"), createFileFromUrl("https://dummyfile.com/files/damage-file-2.jpg")];
        if ($i >= 4) {
            $cmr2->transportedVehicles[] = $vin;
        } else {
            $cmr1->transportedVehicles[] = $vin;
        }
    }

    $creator = new User();

    $creator->id = "605c50daae38f900018baf3f";
    $creator->name = "John Smith";
    $creator->email = "john@brunascustomer.com";

    $carriage->creator = $creator;

    return $carriage;
}

$carriage = fakeCarriage();
switch ($event) {
    case Event::Create:
        makeRequest($url, "POST", Json::encode($carriage), $apiKey);
        break;
    case Event::Update:
        makeRequest($url, "PUT", Json::encode($carriage), $apiKey);
        break;
    case Event::Delete:
        makeRequest("$url/$carriage->id", "DELETE", null, $apiKey);
        break;
}
