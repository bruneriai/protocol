<?php


namespace BrunasProtocol;

use Json\Json;
use Json\JsonDecodeException;

class Parser {
    /**
     * Parses JSON string to BrunasProtocol Carriage object
     * @throws JsonDecodeException
     */
    public static function jsonToCarriage(string $json): Carriage {
        return Json::decode($json, Carriage::class);
    }
}