<?php

namespace BrunasProtocol;

use Json\JsonField;

class Country {
    /**
     * Country code ISO 3166-1 standard alpha-2
     * @var string
     */
    #[JsonField]
    public string $alpha2;
}