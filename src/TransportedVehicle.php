<?php

namespace BrunasProtocol;

use Json\JsonField;
use Json\JsonObjectArray;

class TransportedVehicle {
    /**
     * System generated ID of the TransportedVehicle
     * @var int
     */
    #[JsonField]
    public int $id;

    /**
     * Vehicle identification number
     * @var string
     */
    #[JsonField]
    public string $VIN;

    /**
     * Model of transported vehicle
     * @var string|null
     */
    #[JsonField]
    public ?string $model = null;

    /**
     * Category provided by OEM
     * @var string|null
     */
    #[JsonField]
    public ?string $oemCategory = null;

    /**
     * List of files describing damages on the transported vehicle
     * @var File[]
     */
    #[JsonObjectArray(className: File::class)]
    public array $damageFiles = [];
}