<?php

namespace BrunasProtocol;

use Json\JsonField;
use Json\JsonObjectArray;

class CMR {
    /**
     * System generated ID of the CMR
     * @var int
     */
    #[JsonField]
    public int $id;

    /**
     * ID of task which initiates this CMR (task must exist in same carriage's task list)
     * @var int
     */
    #[JsonField]
    public int $loadTaskId;

    /**
     * ID of task which finishes this CMR (task must exist in same carriage's task list)
     * @var int
     */
    #[JsonField]
    public int $unloadTaskId;

    /**
     * List of files appended to this CMR (usually CMR photos)
     * @var File[]
     */
    #[JsonObjectArray(className: File::class)]
    public array $files = [];

    /**
     * List of TransportedVehicles that is included in this CMR
     * @var TransportedVehicle[]
     */
    #[JsonObjectArray(className: TransportedVehicle::class)]
    public array $transportedVehicles = [];
}