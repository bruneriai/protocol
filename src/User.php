<?php

namespace BrunasProtocol;

use Json\JsonField;

class User {
    /**
     * User ID in Brunas System
     * @var string
     */
    #[JsonField]
    public string $id;


    /**
     * Full user's name
     * @var string
     */
    #[JsonField]
    public string $name;


    /**
     * E-mail of the user
     * @var string|null
     */
    #[JsonField]
    public ?string $email = null;
}