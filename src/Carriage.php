<?php

namespace BrunasProtocol;

use DateTime;
use DateTimeInterface;
use Json\DateTimeInterfaceDecoder;
use Json\JsonField;
use Json\JsonObjectArray;

class Carriage {
    /**
     * Brunas-protocol version
     * @var string
     */
    #[JsonField]
    public string $version = Versions::Current;

    /**
     * System generated ID of the carriage
     * @var int
     */
    #[JsonField]
    public int $id;

    /**
     * Official order number of the carriage (might be auto-generated)
     * @var string
     */
    #[JsonField]
    public string $orderNumber;

    /**
     * Price of the carriage
     * @var Price|null
     */
    #[JsonField]
    public ?Price $price = null;

    /**
     * Expedition data of the carriage (if carriage is sold)
     * @var ExpeditionData|null
     */
    #[JsonField]
    public ?ExpeditionData $expeditionData = null;

    /**
     * Vehicle assigned to execute carriage transportation
     * @var Vehicle
     */
    #[JsonField]
    public Vehicle $assignedVehicle;

    /**
     * List of tasks of which carriage is formed (ordered by execution sequence)
     * @var Task[]
     */
    #[JsonObjectArray(className: Task::class)]
    public array $tasks = [];

    /**
     * List of cmr of which carriage is formed
     * @var CMR[]
     */
    #[JsonObjectArray(className: CMR::class)]
    public array $cmrList = [];

    /**
     * Customer who ordered this carriage
     * @var Company|null
     */
    #[JsonField]
    public ?Company $customer = null;

    /**
     * Start Datetime of carriage (if carriage is not yet started - this value is estimated)
     * @var DateTime
     */
    #[JsonField(decoder: new DateTimeInterfaceDecoder(DateTimeInterface::RFC3339))]
    public DateTime $start;

    /**
     * End Datetime of carriage (if carriage is not yet finished - this value is estimated)
     * @var DateTime
     */
    #[JsonField(decoder: new DateTimeInterfaceDecoder(DateTimeInterface::RFC3339))]
    public DateTime $end;

    /**
     * List of files appended to this carriage (usually order files)
     * @var File[]
     */
    #[JsonObjectArray(className: File::class)]
    public array $files = [];

    /**
     * User who created the carriage (If carriage is created by system, this value will be NULL)
     * @var User|null
     */
    #[JsonField]
    public ?User $creator = null;
}