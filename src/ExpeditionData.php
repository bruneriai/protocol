<?php

namespace BrunasProtocol;

use Json\JsonField;

class ExpeditionData {

    /**
     * Price at which carriage is sold
     * @var Price|null
     */
    #[JsonField]
    public ?Price $price = null;

    /**
     * Company which carries the carriage
     * @var Company|null
     */
    #[JsonField]
    public ?Company $carrier = null;

    /**
     * Invoice file
     * @var File|null
     */
    #[JsonField]
    public ?File $invoiceFile = null;

    /**
     * Order file
     * @var File|null
     */
    #[JsonField]
    public ?File $orderFile = null;
}