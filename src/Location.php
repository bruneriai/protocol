<?php

namespace BrunasProtocol;

use Json\JsonField;

class Location {
    /**
     * Latitude of the location
     * @var float
     */
    #[JsonField]
    public float $lat;
    /**
     * Longitude of the location
     * @var float
     */
    #[JsonField]
    public float $lng;

    /**
     * Address of location
     * @var string
     */
    #[JsonField]
    public string $address;

    /**
     * City of the location
     * @var string
     */
    #[JsonField]
    public string $city;

    /**
     * ZIP code of the location
     * @var string
     */
    #[JsonField]
    public string $zipCode;

    /**
     * country code (ISO 3166-1 standard alpha-2)
     * @var Country
     */
    #[JsonField]
    public Country $country;
}