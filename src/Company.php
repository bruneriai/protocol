<?php

namespace BrunasProtocol;

use Json\JsonField;

class Company {
    /**
     * Brunas system ID of the company
     * @var string
     */
    #[JsonField]
    public string $id;
    /**
     * VAT code of the company
     * @var string
     */
    #[JsonField]
    public string $vatCode;

    /**
     * Company name
     * @var string
     */
    #[JsonField]
    public string $name;
}