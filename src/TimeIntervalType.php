<?php

namespace BrunasProtocol;
enum TimeIntervalType: string {
    case At = "at";
    case After = "after";
    case Before = "before";
    case Between = "between";
}