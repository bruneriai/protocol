<?php

namespace BrunasProtocol;

enum TaskType: string {
    case Load = "load";
    case Unload = "unload";
    case FuelStation = "fuel-station";
    case CarWash = "car-wash";
    case Service = "service";
    case DriverSwap = "driver-swap";
    case TrailerSwap = "trailer-swap";
    case Parking = "parking";
    case Customs = "customs";
    case TechnicalInspection = "technical-inspection";
    case Ferry = "ferry";
    case DocumentTransfer = "document-transfer";
    case Train = "train";
    case Tunnel = "tunnel";
    case TruckSwap = "truck-swap";
    case PalletStorage = "pallet-storage";
    case ContainerPickUp = "container-pick-up";
    case ContainerDropAt = "container-drop-at";
    case TrailerCouple = "trailer-couple";
    case TrailerUncouple = "trailer-uncouple";
}