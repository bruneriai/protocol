<?php

namespace BrunasProtocol;

use Json\JsonField;

class File {
    /**
     * Public url to access file
     * @var string
     */
    #[JsonField]
    public string $publicUrl;
}