<?php

namespace BrunasProtocol;

use DateTime;
use DateTimeInterface;
use Json\DateTimeInterfaceDecoder;
use Json\JsonField;

class Task{
    /**
     * System generated ID of the Task
     * @var int
     */
    #[JsonField]
    public int $id;

    /**
     * Location of task destination
     * @var Location
     */
    #[JsonField]
    public Location $location;

    /**
     * Task type describing what is the purpose of the task
     * @var TaskType
     */
    #[JsonField]
    public TaskType $type;

    /**
     * date at which task should be executed (format YYYY-MM-DD)
     * @var string
     */
    #[JsonField]
    public string $date;

    /**
     * Time at which task should be executed
     * @var TimeInterval|null
     */
    #[JsonField]
    public ?TimeInterval $validArrivalTime = null;

    /**
     * Manager-written comment for driver regarding this task
     * @var string
     */
    #[JsonField]
    public string $commentForDriver;

    /**
     * Date time of vehicle arriving at task location
     * @var DateTime|null
     */
    #[JsonField(decoder: new DateTimeInterfaceDecoder(DateTimeInterface::RFC3339))]
    public ?DateTime $arrivalTime = null;

    /**
     * Date time of vehicle departing from task location
     * @var DateTime|null
     */
    #[JsonField(decoder: new DateTimeInterfaceDecoder(DateTimeInterface::RFC3339))]
    public ?DateTime $departureTime = null;



}