<?php

namespace BrunasProtocol;

use Json\JsonField;

class TimeInterval {
    /**
     * This type described how whole object must be interpreted:
     * at - (exactly at $from) f.e. exactly at 12:00
     * before - (before $to) f.e. before 15:00
     * after (after $from) f.e. after 08:00
     * between (from $from to $to) f.e. 08:00 - 16:00
     * @var TimeIntervalType
     */
    #[JsonField]
    public TimeIntervalType $type;

    /**
     * Format HH:mm
     * @var string|null
     */
    #[JsonField]
    public ?string $from = null;

    /**
     * Format HH:mm
     * @var string|null
     */
    #[JsonField]
    public ?string $to = null;
}