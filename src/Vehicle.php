<?php

namespace BrunasProtocol;

use Json\JsonField;

class Vehicle{
    /**
     * System generated ID of the Vehicle
     * @var int
     */
    #[JsonField]
    public int $id;
    /**
     * Vehicle registration number
     * @var string
     */
    #[JsonField]
    public string $registrationNumber;
}