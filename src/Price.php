<?php

namespace BrunasProtocol;

use Json\JsonField;

class Price {
    /**
     * Fixed price in euros
     * @var float
     */
    #[JsonField]
    public float $price;
}